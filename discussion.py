# Python also allows user to input, with this, users can give inputs to the program

# [Section] input
#----------------------------------------------------------------------------------------------------------
# username = input("Please enter your name: \n")
    # \n para mag new line lang or nasa baba lang ang input
# print(f"Hello {username}! Welcome to python short course!")


# num1 = int(input("Enter first number: "))
# num2 = int(input("Enter second number: "))
# print(f"The sum of num1 and num2 is {num1+num2}")
#----------------------------------------------------------------------------------------------------------

# [Section] With user inputs, users can give inputs for the program to be used to control the application using control structures
# Control structures can be divided into selection and repitition structures
# repetition    - number of loops
# selection     - condition
# Selection control structure allows the program to choose among choices and run specific codes depending on the choice taken(conditions)
# Repetition control structure allows the program to repeat certatin blocks of code given a starting condition and termination condition

# [Section] If-else statements
# if-else statements are used to choose between two or more code blocks depending on the condition
test_num = 75

if test_num >= 60 : 
    print("Test passed!")
else:
    print("Test failed")
#----------------------------------------------------------------------------------------------------------
# If I want na may input and user
test_num = int(input("Please enter your score:"))

if test_num >= 60 : 
    print("Test passed!")
else:
    print("Test failed")
#----------------------------------------------------------------------------------------------------------
# Note that in Python, curly braces ({}) are not needed to distinguise the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed

# [Section] if elsechains can also be used to have more then 2 choices for the program

test_num2 = int(input("Please enter the second number:"))

if test_num2 >0 :
    print("The number is positive")
    print(f"The number is {test_num2}")
elif test_num2 ==0 :
    print("The number is equal to 0")
else:
    print("The number is negative")

# Mini-exercise
#----------------------------------------------------------------------------------------------------------
# num = int(input("Enter a number:"))

##! IF THE NUMBER IS 15 - DIVISBLE BY 3 LANG ANG ANSWER BECAUSE SA FIRST RUN PA LANG NAGSTOP NA !##
# if num%3==0 :
#     print("The number is divisible by 3")
# elif num%5==0 :
#     print("The number is divisible by 5")
# elif num%3==0 & num%5==0:
#     print("The number is divisible by both 3 and 5")
# else :
#     print("The number is not divisible by 3 nor 5")
#----------------------------------------------------------------------------------------------------------
# num = int(input("Enter a number:"))

# if num%3==0 & num%5==0:
#     print("The number is divisible by both 3 and 5")
# elif num%3==0 :
#     print("The number is divisible by 3")
# else :
#     print("The number is not divisible by 3 nor 5")
#----------------------------------------------------------------------------------------------------------

# [Section] Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statements as long as the condition is true
i = 5

while i<=5 :
    print(f"Current value of i is {i}")
    i+=1

# [Section] for loops are used for iteration over a sequence
fruits = ["apple", "banana", "cherry"] #these are called lists instead of array(Javascript) 

# for(Python) is like foreach/map(JS) 
for indiv_fruit in fruits :  
    print(indiv_fruit)

# [Section] range() method
# To use the for loop to iterate through calues, the range method can be used 

# default starting value is 0
# (stops_at_this_number)
for x in range(6) :
    print(f"The current value of x is {x}")

# (start, end_before)
for x in range(6, 10) :
    print(f"The current value of x is {x}!")

# (start, end_before, increment)
for x in range(6, 20, 2) :
    print(f"The current value of x is {x}~")

# [Section] Break Statement
# The break statement is used to stop the loop

j=1
while j<6 :
    print(j)

    if j==3 :
        break
    j+=1
